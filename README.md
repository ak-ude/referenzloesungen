# Tutoring Team The Swarm Project

Altklausuren sind für jeden Studenten in jedem Semester ein Dauerbrenner. Oft sitzt man zuhause vor Altklausuren, versucht diese zu lösen oder hat eine Lösung und möchte mit anderen vergleichen ob die eigenen Ergebnisse stimmen. Oft fehlt es aber an einer Referenzlösung mit der man seine Lösung vergleichen kann.

Dieses Problem möchte das The Swarm Project lösen. Möglichst viele Studenten sollen sich an alten Klausuren versuchen. Jeder wie er kann und Zeit hat. Völlig egal ob eine ganze Klausur oder nur eine Teilaufgabe, jede Hilfe ist herzlich willkommen.

Mit einem Schwarm aus Studenten, von denen Jeder mithilft, können wir gemeinsam ein Archiv mit Referenzlösungen für alte Klausuren auf die Beine stellen. Je größer das Archiv wird, desto größer ist der Nutzen für jeden Einzelnen von euch.

## Was habe ich davon?

Das The Swarm Project dient dazu möglichst viele Lösungen für Altklausuren in Latex zu erstellen.

Wer sich daran beteiligt, übt schonmal Latex einzusetzen. Latex ist ein solides Werkzeug, dass beim Erstellen der eigenen Thesis oder eines Praxisberichts extrem nützlich sein kann. Das Lösen einer Altklausur lässt dich den Stoff nocheinmal über das Semester hinaus üben.

Alle, die sich konzentriert auf eine Altklausur vorbereiten wollen, können hier ihre eigenen Lösungen überprüfen und ggf. verbessern.

Zusätzlich wird hier der Umgang mit Git geübt, was ein wichtiges Werkzeug für die Softwareentwicklung darstellt. Sei es für den späteren Job oder nur das Wahlprojekt im 5. Semester. Wer sich nicht noch zusätzlich zum Programmieren mit den notwendigen Werkzeugen beschäftigen muss, kann richtig durchstarten.

## Wie kann ich mitmachen?

1. Erstelle einen GitLab Account
2. Schicke eine [E-Mail an die Productowner](mailto:theswarmproject@outlook.com) mit deinem Accountnamen, um die Rolle **Maintainer** zu erhalten.
3. Clone das Repository und erstelle einen neuen branch.
4. Löse eine Alt- oder Probeklausur und schreibe diese in Latex auf.
5. Eerstelle eine Merge-Request und füge eine Person mit der Rolle **Owner** als Reviewer hinzu.

# Codestyle

- Leerzeichen in Dateinamen sind nicht zulässig, das Makeskript funktioniert sonst nicht korrekt! Leerzeichen in Dateinamen sind mit Unter- oder Bindestrich zu schreiben.
- Lösungen _müssen_ in Latex geschrieben sein.
- Der/Die Autor/en müssen mit Pseudonym oder Klarnamen in der Lösung genannt werden.
- Das unter `src/lib` zu findende Logo und Latex Template ist zu verwenden.
- Das Logo darf bei einem Fork getauscht werden.
- Für ein eigenes Logo muss das Urheberrecht am Logo eigenständig geklärt werden.
- Per Default soll immer das Tutoring-Team-Logo verwendet werden.
- Es dürfen nur Latex-Dateien werden.
- Lediglich Tabellen, Zeichnungen und Diagramme müssen nicht in Latex gecodet werden. Diese dürfen als Screenshot eingebunden werden.
- Kompilierte PDF-Dateien dürfen nicht eingecheckt werden.
- Folgende Dateitypen sind explizit nicht erlaubt: `doc`, `docx`
- Lösungen zu einer Aufgabe müssen mit `\section*{Aufgabe NUMMER}` eingeleitet werden.
- Teilaufgaben müssen mit `\subsection*{a,b,c.....)}` kenntlich gemacht werden.
- Lösungen sollen verständlich und nachvollziehbar notiert werden.

# Definition of Done (DOD)

Beim bearbeiten eines Issues auf dem Kanbanboard ist wie folgt vorzugehen: - Eine Karte aus dem Bereich TODO nehmen und in 'In Progress' verschieben. - Sich selbst die Karte als Aufgabe zuweisen. - Den Issue bearbeiten. - Einen Pull-Request mit der Lösung öffnen und einen zweiten Entwickler als Reviewer anfragen. - Sobald der Pull-Request in den main Branch gemerged wurde darf/muss das Issue in Done verschoben werden.

Beim Review eines PR ist darauf zu achten, dass die Aufgabe wie spezifiziert erledigt wurde. Es dürfen keine Binaries im PR vorhanden sein. Die Aufgabe ist vollständig erledigt sobald sie gelöst, durch einen Review überprüft wurde und der Reviewer keine Kommentare an den PR mehr angehängt. Außerdem müssen alle angehengten Kommentare an einem PR aufgelöst werden.

# Lizenz

Das Projekt und sein Inhalt sind lizenzfrei.
Aus Lizenzgründen darf der Inhalt der gelösten Altklausur nicht abgeschrieben oder anders in die Lösung eingebunden werden.

# Disclaimer

Es wird explizit darauf hingewiesen, dass es sich hier um Lösungen von Studenten für Studenten handelt. **Es sind keine Musterlösungen.** Wer einen Fehler in einer Lösung findet, ist eingeladen diesen zu korrigieren und einen Merge-Request anzulegen.

# Und die Altklausuren selbst?

Das Tutoring-Team bietet in seiner Öffnungszeit die Möglichkeit Altklausuren in Papierform zu erhalten.
