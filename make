#!/bin/bash

# Useful shortcuts
start_dir="$PWD"
out_dir="$PWD/out"
tex_dir="$PWD/src/texfiles"

# Purging => Remove everything from out_dir and add the Placeholder.pdf
if [[ $1 == "purge" ]]; then
    rm -f $out_dir/*
    touch $out_dir/Placeholder.pdf
    exit
fi

# Iterate over each module folder
modules=$(find $tex_dir -mindepth 1 -maxdepth 1 -type d)
for module in $modules; do
    # Ensure references in .tex files can remain the same
    cd $module 

    # Grab module folder name
    module_name=$(basename $module)
    echo "Module: $module_name"
    
    # Iterate over each .tex file
    texs=$(find $module -mindepth 1 -maxdepth 1 -type f -name "*.tex")
    for tex in $texs; do

        # Grab .tex file name
        tex_name=$(basename ${tex%%.*})

        # Generate the corresponding PDF name
        pdf_name="${module_name^^}""_$tex_name"
        
        # Ensure the .tex file is newer than the PDF / PDF doesn't exist
        if [ "$tex" -nt "$out_dir/$pdf_name.pdf" ]; then
            rm -f "$out_dir/$pdf_name.pdf"
            echo -n " => Compiling: $tex_name... "
    
            # Compile with pdflatex
            pdflatex \
                -jobname "$pdf_name" \
                -output-directory "$out_dir" \
                -interaction batchmode \
                "$tex" >> /dev/null

            # Check compilation status and output colorful feedback
            if test -f "$out_dir/$pdf_name.pdf"; then
                printf '\e[32m%s\e[0m\n' "Done"
            else
                printf '\e[31m%s\e[0m\n' "ERROR: Could not compile!"
            fi
        fi
    done
    cd $start_dir # Go back home
done

# In release mode, all logs get removed from out_dir
if [[ $1 == "release" ]]; then
    rm -f out/*.log out/*.aux out/*.out
fi

